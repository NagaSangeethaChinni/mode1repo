package com.hcl.model;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserCodeModel14 {
	
	   public static Date convertDateFormate(String str) throws ParseException {
	     
	      SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
	     
	      Date date = formatter.parse(str);
	     
	      return date;
	   }
}
