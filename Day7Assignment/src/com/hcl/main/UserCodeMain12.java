package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.UserCodeModel12;

public class UserCodeMain12 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Ip Address:");

		String ipAddress = sc.nextLine();
		
		boolean b = UserCodeModel12.ipValidator(ipAddress);
		if (b == true)
			System.out.println("Valid");
		else
			System.out.println("Invalid");
	}

}
