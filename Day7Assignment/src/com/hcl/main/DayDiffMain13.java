package com.hcl.main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

import com.hcl.model.DayDifferent13;

public class DayDiffMain13 {

	public static void main(String[] args) throws IOException, ParseException {
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.println("Enter first date");
		    String s1 = br.readLine();
		    System.out.println("Enter second date");
		    String s2 = br.readLine();
		    System.out.println(DayDifferent13.getDateDifference(s1, s2));

	}

}

