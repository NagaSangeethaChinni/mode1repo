package com.hcl.model;

public class Square extends Shape {
	private int side;
	private float area;

	@Override
	public float calculateArea(int side) {
		return area = side * side;

	}

	public Square(String square) {
		super(square);
	}

	public Square(String name, int side) {
		super(name);
		this.side = side;
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

	@Override
	public float calculateArea() {
		// TODO Auto-generated method stub
		return 0;
	}

}

