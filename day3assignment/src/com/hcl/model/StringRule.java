package com.hcl.model;


/*Write a program to read a string and return a modified string 
 * based on the following rules. 
Return the String without the first 2 characters except when 
Keep the first char if it is 'k' 
Keep the second char if it is 'b'. */
public class StringRule {

	public String remove(String str) {
		if (str.charAt(0) == 'k') {
			str = str.substring(2, str.length());
			return 'k' + str;

		} else if (str.charAt(1) == 'b') {
			str = str.substring(1, str.length());

		} else {

			str = str.substring(2, str.length());
		}

		return str;
	}

}


