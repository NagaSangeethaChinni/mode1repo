package com.hcl.model;
/* Write a Java program to replace all the 'd' occurrence characters with �h� characters in each string.
*/

public class Replace {
	public String replaceChar(String str){
		String new_str = str.replace('d','h');
		System.out.println("Original string: "+str);
		return new_str;
		
	}

}