package com.hcl.main;

/*Write a program to read a string and a character, 
 * and reverse the string and convert it in a format such 
 * that each character is separated by the given character. 
 * Print the final string. */
import java.util.Scanner;

import com.hcl.model.ReverseString;

public class ReverseMain {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		ReverseString reverseString = new ReverseString();

		System.out.println("Enter the String:");
		String str = scanner.next();

		System.out.println("Reversed String is:" + reverseString.wordReverse(str));

	}

}
