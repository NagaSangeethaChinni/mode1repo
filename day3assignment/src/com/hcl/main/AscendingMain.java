package com.hcl.main;

import com.hcl.model.Ascending;

public class AscendingMain {
public static void main(String[] args){
	int[] obj = new int[] {-5,-9,8,12,1,3};
	System.out.println("Elements of given Array");
	
	Ascending.printArray(obj);
	Ascending.sortArray(obj);
	
}
}
