package com.hcl.main;

import com.hcl.model.Palindrome;

public class PalindromeMain {

	public static void main(String[] args) {
		Palindrome palindrome = new Palindrome();
		String str = "madam";
		String temp = palindrome.checkPalindrome(str);
		System.out.println("given string is:" + str);
		System.out.println("reversed string is:" + temp);

		if (temp.equals(str)) {
			System.out.println("yes");
		} else {
			System.out.println("no");
		}
	}

}

