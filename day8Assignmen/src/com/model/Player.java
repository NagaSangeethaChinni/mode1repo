package com.model;

public class Player {
	String name;
	String teamName;
	int matches;
	public Player() {
		super();
	}
	public Player(String name, String teamName, int matches) {
		super();
		this.name = name;
		this.teamName = teamName;
		this.matches = matches;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public int getMatches() {
		return matches;
	}
	public void setMatches(int matches) {
		this.matches = matches;
	}
	

}
