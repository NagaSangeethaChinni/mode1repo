package com.hcl.main;


import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToFile {

	public static void main(String[] args) {
		String data = "Data to be written on a file";
		char[] array = new char[100];
		try {
			FileWriter output = new FileWriter("E:\\Hcl training\\myfolder\\file2.txt");
			output.write(data);
			System.out.println("Success");
			output.close();
			FileReader input = new FileReader("E:\\Hcl training\\myfolder\\abc.txt");
			input.read(array);
			System.out.println(array);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
