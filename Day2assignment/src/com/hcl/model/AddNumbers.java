package com.hcl.model;
/*Your task is to create the class Addition and the required methods so that the code prints  
the sum of the numbers passed to the function addition.*/ 

import java.util.Scanner;

public class AddNumbers {

	public static void main(String[] args) {

		int n;
		int sum = 0;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter no. of elements you want in array:");
		n = s.nextInt();
		int a[] = new int[n];
		System.out.println("Enter all the elements:");
		for (int i = 0; i < n; i++) {
			a[i] = s.nextInt();
			sum = sum + a[i];
		}
		System.out.println("Sum:" + sum);
	}

}
