package com.hcl.model;

public class Smallest {
	public static int findSmallest(int number1, int number2, int number3) {

		return Math.min(Math.min(number1, number2), number3);
	}

	/*
	 * int temp = 0 ;
	 * 
	 * if((number1 < number2) && (number1 < number3)){
	 * 
	 * temp = number1; }else{ if((number2 < number1) && (number2 <number3)){
	 * temp = number2;
	 * 
	 * }else{ temp = number3;
	 * 
	 * } } return temp; }
	 */

}
