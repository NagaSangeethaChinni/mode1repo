package com.hcl.model;

public class A {
	private int empno;
	private String empname;
	public A() {
		super();
	}
	public A(int empno, String empname) {
		super();
		this.empno = empno;
		this.empname = empname;
	}
	public int getEmpno() {
		return empno;
	}
	public void setEmpno(int empno) {
		this.empno = empno;
	}
	public String getEmpname() {
		return empname;
	}
	public  void setEmpname(String empname) {
		this.empname = empname;
	}

}
