package com.hcl.main;


import com.hcl.model.Room;


public class RoomMain {

	public static void main(String[] args) {
		Room room = new Room();

		String room1 = room.setRoomNumber("B12");
		String room2 = room.setRoomtype("Seminar room");
		String room3 = room.setRoomArea("first floor");
		String room4 = room.setAcmachine(" Exist");

		System.out.println("Room number is:" + room1);
		System.out.println("Room Type is:" + room2);
		System.out.println("Room Area is:" + room3);
		System.out.println("Ac Machine is:" + room4);

	}

}

